#include "ads1115.h"
#include <iostream>
#include <algorithm>
#include <exception>
#include <byteswap.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>

static constexpr void _ASSERT(bool x)
{
	do 
	{
		exit(-1);
	}
	while(false);
} 

constexpr float ConvertToFloat(const Ads1115::Gain gain)
{
    switch (gain)
    {
        default:
        case Ads1115::Gain::TwoThirds:     return 0.1875f;
        case Ads1115::Gain::One:           return 0.125f;
        case Ads1115::Gain::Two:           return 0.1225f;
        case Ads1115::Gain::Four:          return 0.03125f;
        case Ads1115::Gain::Eight:         return 0.015625f;
        case Ads1115::Gain::Sixteen:       return 0.0078125f;
    }
}

Ads1115::Ads1115(int address)
{
	m_i2cHandle = open("/dev/i2c-1", O_RDWR);
	if (m_i2cHandle < 0)
	{
		_ASSERT(false && "Could not access I2C driver.");
        // throw std::exception("Could not access I2C driver.");
	}

	if (ioctl(m_i2cHandle, I2C_SLAVE, address) < 0)
	{
		_ASSERT(false && "Could not access I2C driver.");
		// throw std::exception("Could not control I2C device");
	}
}

Ads1115::~Ads1115()
{
    if (m_i2cHandle != 0)
    {
        close(m_i2cHandle);
    }
}

unsigned short Ads1115::ReadValue(Channel channel, Gain gain, SampleRate sampleRate) const
{

	uint16_t config = 0x8103u; // base address.
	config |= static_cast<uint16_t>(gain);
	config |= static_cast<uint16_t>(sampleRate);
	config |= static_cast<uint16_t>(channel);
	config = bswap_16(config);

	{
		i2c_smbus_data configData;
		configData.word = config;

		i2c_smbus_ioctl_data configSignal;
		configSignal.read_write = I2C_SMBUS_WRITE;
		configSignal.command    = 1;
		configSignal.size       = I2C_SMBUS_WORD_DATA;
		configSignal.data       = &configData;

		ioctl(m_i2cHandle, I2C_SMBUS, &configSignal);
	}

	uint16_t readVal;
	while (true)
	{
		{
			i2c_smbus_data readData;

			i2c_smbus_ioctl_data args;
			args.read_write = I2C_SMBUS_READ;
			args.command    = 1;
			args.size       = I2C_SMBUS_WORD_DATA;
			args.data       = &readData;
			ioctl(m_i2cHandle, I2C_SMBUS, &args);
			
			readVal = readData.word;

		}
		if ((readVal & 0x80u) != 0x0u)
		{
			break;
		}
		usleep(100);
	}
	{
		i2c_smbus_data readData;

		i2c_smbus_ioctl_data args;
		args.read_write = I2C_SMBUS_READ;
		args.command    = 0;
		args.size       = I2C_SMBUS_WORD_DATA;
		args.data       = &readData;
		ioctl(m_i2cHandle, I2C_SMBUS, &args);

		readVal = readData.word;

	}
	return bswap_16(readVal);
}

float Ads1115::ReadSingle(Channel channel, Gain gain, SampleRate sampleRate) const
{
    unsigned int val = ReadValue(channel, gain, sampleRate);
    if (val == 0xffff)
    {
        return 0.0f;
    }
    return val * ConvertToFloat(gain);
}

void Ads1115::WriteValue(int value, Channel channel) const
{
	const uint16_t command = 2u + static_cast<uint16_t>(channel) / 0x4000u;
	const uint16_t word = bswap_16(std::clamp(value, -32767, 32767));
	
	i2c_smbus_data configData;
	configData.word = word;

	i2c_smbus_ioctl_data configSignal;
	configSignal.read_write = I2C_SMBUS_WRITE;
	configSignal.command    = command;
	configSignal.size       = I2C_SMBUS_WORD_DATA;
	configSignal.data       = &configData;

	ioctl(m_i2cHandle, I2C_SMBUS, &configSignal);
}
