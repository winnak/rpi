#include <iostream>

#include "ads1115.h"

int main(int argc, char** argv)
{
	Ads1115 ads{0x48};
	for (int i = 0; i < 4; i++)
	{
		unsigned short readVal = ads.ReadValue(i);
		float readfloat = ads.ReadSingle(i);
		printf("%i: readVal = %05u   0x%05x   %5f\n", i, readVal, readVal, readfloat * 0.001f);
	}

	return 0;
}
