#pragma once

class Ads1115
{
public:
	enum class Channel : unsigned short
	{
		// Diff0 = 0 << 12,
		// Diff1 = 1 << 12,
		// Diff2 = 2 << 12,
		// Diff3 = 3 << 12,
		A0    = 0x4000,
		A1    = 0x5000,
		A2    = 0x6000,
		A3    = 0x7000,
	};
	enum class Gain : unsigned short
	{
		// The ADC input range (or gain) can be changed via the following
		// functions, but be careful never to exceed VDD +0.3V max, or to
		// exceed the upper and lower limits if you adjust the input range!
		// Setting these values incorrectly may destroy your ADC!
		//
		//                 FSR            LSB SIZE
		// 2/3x gain +/- 6.144V  1 bit = 0.1875mV (default)
		//   1x gain +/- 4.096V  1 bit = 0.125mV
		//   2x gain +/- 2.048V  1 bit = 0.0625mV
		//   4x gain +/- 1.024V  1 bit = 0.03125mV
		//   8x gain +/- 0.512V  1 bit = 0.015625mV
		//  16x gain +/- 0.256V  1 bit = 0.0078125mV
		TwoThirds  =  0x0000, // 1 << 9,   // 0x0000
		One        =  0x0200, // 2 << 9,   // 0x0200
		Two        =  0x0400, // 3 << 9,   // 0x0400
		Four       =  0x0600, // 4 << 9,   // 0x0600
		Eight      =  0x0800, // 5 << 9,   // 0x0800
		Sixteen    =  0x0A00, // 6 << 9    // 0x0A00
	};
	enum class SampleRate : unsigned short
	{
		S8   = 0x0000,
		S16  = 0x0020,
		S32  = 0x0040,
		S64  = 0x0060,
		S128 = 0x0080,
		S475 = 0x00A0,
		S860 = 0x00C0,
	};

	Ads1115(int address = 0x48);
	~Ads1115();

	unsigned short ReadValue(Channel channel, Gain gain = Gain::TwoThirds, SampleRate sampleRate = SampleRate::S128) const;
	inline unsigned short ReadValue(unsigned char channel, Gain gain = Gain::TwoThirds, SampleRate sampleRate = SampleRate::S128) const
	{
		if (channel > 4)
		{
			return 0;
		}
		
		return ReadValue(static_cast<Channel>((channel + 4) * 0x1000), gain, sampleRate);
	}
	float ReadSingle(Channel channel, Gain gain = Gain::TwoThirds, SampleRate sampleRate = SampleRate::S128) const;
	inline float ReadSingle(unsigned char channel, Gain gain = Gain::TwoThirds, SampleRate sampleRate = SampleRate::S128) const
	{
		if (channel > 4)
		{
			return 0.0f;
		}
		
		return ReadSingle(static_cast<Channel>((channel + 4) * 0x1000), gain, sampleRate);
	}

	void WriteValue(int value, Channel channel) const;

    int m_i2cHandle;
};