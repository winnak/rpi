#include <iostream>
#include <string_view>
#include <gpiod.h>
#include <unistd.h>
#include <errno.h>

static std::string_view k_GpioDevice0 = "gpiochip0";
static std::string_view k_GpioDevice1 = "gpiochip1";

static constexpr int on = 1;
static constexpr int off = 0;

static void PrintDeviceLines()
{
	gpiod_chip_iter* itChip = gpiod_chip_iter_new();
	
	gpiod_chip* chip;
	gpiod_foreach_chip(itChip, chip)
	{
		std::cout << "Chip " << gpiod_chip_label(chip) << " (" << gpiod_chip_name(chip) << ")" << std::endl;
		gpiod_line* line;
		gpiod_line_iter* itLine = gpiod_line_iter_new(chip);
		gpiod_foreach_line(itLine, line)
		{
			const char* name = gpiod_line_name(line);
			const char* io = gpiod_line_direction(line) == GPIOD_LINE_DIRECTION_INPUT ? " [I]" : " [O]";
			int offset = gpiod_line_offset(line);
			std::cout << "  " << offset << " " << name << io << std::endl;
		}
		gpiod_line_iter_free(itLine);
	}

	gpiod_chip_iter_free(itChip);
}

int main(int argc, char** argv)
{
	const char* version = gpiod_version_string();
	std::cout << "GPIO Version: " << version << std::endl;
	
	PrintDeviceLines();

	gpiod_chip* device = gpiod_chip_open_by_name(k_GpioDevice0.data());
	gpiod_line* line = gpiod_line_find("GPIO17");

	if (gpiod_line_update(line) == -1)
	{
		std::cerr << "Fail 1. Error: " << errno << std::endl;
	}

	if (gpiod_line_request_output(line, "flower", 0) != 0)
	{
		std::cerr << "Failed to request line. Error: " << errno << std::endl;
		return 1;
	}

	if (gpiod_line_set_value(line, on) == -1)
	{
		std::cerr << "Failed to set value. Error: " << errno << std::endl;
		return 1;
	}

	std::cout << "Sending signal";
	sleep(6);
	
	gpiod_line_set_value(line, off);

	gpiod_line_release(line);

	gpiod_chip_close(device);

	return 0;
}
