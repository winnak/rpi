@echo off
if ["%~1"]==[""] (
    echo Usage: %0 ^<project path^>
    goto :eof
)

echo ====== Uploading ======
call upload %1
if errorlevel 1 goto error
echo.
echo ====== Compiling ======
call compile %*
if errorlevel 1 goto error
echo.
echo ====== Executing ======
call run %*
if errorlevel 1 goto error
goto :eof
:error
echo.
echo ======== ERROR ========
