# Rpio docs

Goal: Aspect oriented Raspberry Pi IO

Which creates an accessability pattern that verifies that it is possible at compile time.

```cpp
RaspberryIO<                  // Create the Raspberry Pi Object, with 2 aspects
   Multiplexer<               // Aspect 1: Multiplexer: figures out
      Pin::Gpio14,           //   Multiplexer: Selector pin
      Ads1115,               //   Reserves Gpio 2 and 3 for the ADC chip
      Ads1115<Pin::Gpio5, Pin::Gpio4> //   Same as above + Gpio4 for the optional address and Gpio5 for the optional alert.
   >, Gpio<Pin::Gpio3>>       // Aspect 2: Gpio3 is being used for simple read/write to pin 3.
rpio;                         // store in rpio variable
```

This should give a compile error since Ads1115 uses the Gpio3, so it conflicts.

But say it was Gpio6 instead of Gpio3:

```cpp
// Print out how to wire the components.
rpio.PrintWiring();

bool result = rpio.gpio6.Read(); // false!
rpio.gpio6.Write(!result);
result = rpio.gpio6.Read(); // true!

// Multiplexer Component
auto* selected1 = rpio.multiplexer14.Select(1);
selected1->ReadHalf();
auto* selected0 = rpio.multiplexer14.Select(0);
selected1->ReadHalf(); // nullptr exception, we have selected something else, so this can no longer be read.
selected0->ReadHalf(); // works fine.
```

## TODO

Need some error handling. Perhaps through the webserver.

