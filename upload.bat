@echo off
if ["%~1"]==[""] (
    echo Usage: %0 ^<project path^>
    goto :eof
)

for /f %%i in (pi_target) do SET TARGET=%%i

scp -r %1/ %TARGET%:projects/
