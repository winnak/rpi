@echo off
if ["%~1"]==[""] (
    echo Usage: %0 ^<project path^>
    goto :eof
)

echo Arguments: %*
for /f "tokens=1,* delims= " %%a in ("%*") do set ALL_BUT_FIRST=%%b

for /f %%i in (pi_target) do SET TARGET=%%i

ssh %TARGET% "make -C projects/%1/ %ALL_BUT_FIRST%"
